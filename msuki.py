from selenium import webdriver
import time
import random
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager

#LINUX
#driver = webdriver.Chrome()

#WINDWOS
driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get('https://web.whatsapp.com/')

def mode1():

    name = input('[!] Inserisci il nome della persona o del gruppo : ')
    msg = input('[!] Inserisci il messaggio da inviare : ')
    count = float(input('[!] Ogni quanti minuti vuoi inviarlo? : '))
    count = count*60

    input('Premi un tasto qualsiasi (dopo aver scannerizzato QR code) per iniziare..')

    user = driver.find_element_by_xpath('//span[@title = "{}"]'.format(name))
    user.click()

    msg_box = driver.find_element_by_class_name('_1Plpp')


    while True:

        msg_box = driver.find_element_by_class_name('_1Plpp')
        msg_box.send_keys(msg)

        #parte per cambiare bottone da registra a invia scrivendo dentro qualcosa a caso
        try:
            button = driver.find_element_by_class_name('_35EW6')
        except:
            #scrivi nella barra
            msg_box.send_keys(msg)
            #aspetta che il bottone passi da registra audio a invia perchè ha scritto nella barra
            time.sleep(1)
            button = driver.find_element_by_class_name('_35EW6')

        button.click()
        time.sleep(count)


    return

def mode2():

    lista1 = ["wow", "Wow", "We", "Buongiorno", "Giorno"]
    lista2 = ["Maik", "Maiki", "maik", "maiki", "msuki", "Msuki", "Michael", ""]
    lista3 = ["hai votato", "ricordati di votare", "quanti mancano", "chi non ha votato", "quanto manca"]
    lista4 = ["?", "??", "???", ".", "!?", "?!", ""]

    listam1 = ["Mucu", "mucu", "mucculini", "", "Muq", "Marco"]
    listam2 = ["sei", "per me sei", "cristo se sei", "minchia se sei", "ma quanto sei", "sei davvero"]
    listam3 = ["uno sbiocco", "uno schifo", "un tamberlo", "un coglione", "un pezzo di merda", "un fascio", "un fascista", "un nazi"]
    listam4 = ["schifoso", "lardoso", "di merda", "porcodio", "dio can", "vegn", "dai", "cristo"]

    name = input('[!] Inserisci il nome della persona o del gruppo : ')
    count = float(input('[!] Ogni quanti minuti vuoi inviarlo? : '))
    count = count*60

    input('Premi un tasto qualsiasi (dopo aver scannerizzato QR code) per iniziare..')

    user = driver.find_element_by_xpath('//span[@title = "{}"]'.format(name))
    user.click()

    msg_box = driver.find_element_by_class_name('_1Plpp')

    while True:

     

        #parte per cambiare bottone da registra a invia scrivendo dentro qualcosa a caso
        try:
            msg_box = driver.find_element_by_class_name('_1Plpp')
            msg_box.send_keys(random.choice(lista1) + " " + random.choice(lista2) + " " + random.choice(lista3) + random.choice(lista4))
            button = driver.find_element_by_class_name('_35EW6')
        except:
            #scrivi nella barra
            #msg_box.send_keys(random.choice(lista1) + " " + random.choice(lista2) + " " + random.choice(lista3) + random.choice(lista4))
            #aspetta che il bottone passi da registra audio a invia perchè ha scritto nella barra
            msg_box = driver.find_element_by_class_name('_1Plpp')
            msg_box.send_keys(random.choice(lista1) + " " + random.choice(lista2) + " " + random.choice(lista3) + random.choice(lista4))
            time.sleep(1)
            button = driver.find_element_by_class_name('_35EW6')

        button.click()
        time.sleep(count)

    return

print('   *     (               )  (')                                                       
print(' (  `    )\ )         ( /(  )\ )              )           )')                         
print(' )\))(  (()/(    (    )\())(()/(   (  (    ( /(     )  ( /(        )')                
print('((_)()\  /(_))   )\ |((_)\  /(_))  )\))(   )\()) ( /(  )\())(   ( /(  `  )   `  )')   
print('(_()((_)(_))  _ ((_)|_ ((_)(_))   ((_)()\ ((_)\  )(_))(_))/ )\  )(_)) /(/(   /(/(')   
print('|  \/  |/ __|| | | || |/ / |_ _|  _(()((_)| |(_)((_)_ | |_ ((_)((_)_ ((_)_\ ((_)_\ ')  
print('| |\/| |\__ \| |_| |  |  <   | |  \ V  V /| | \ / _` ||  _|(_-</ _` || |_ \)| |_ \) ') 
print('|_|  |_||___/ \___/  _|\_\ |___|   \_/\_/ |_||_|\__,_| \__|/__/\__,_|| .__/ | .__/  ')
print('                                                                     |_|    |_|  ')   


print("[!] Scegli la modalità: ")
print("[1] Messaggio ripetuto ")
print("[2] Messaggi random ")
print("[0] Esci")


while True:

    scelta = str(input(">>> "))

    if scelta == '1':
        mode1()
    elif scelta == '2':
        mode2()
    elif scelta == '0':
        exit()
    else:
        print("[!] Scelta non valida! Riprova")




