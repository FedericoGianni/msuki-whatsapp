# msuki-whatsapp

con msuki-whatsapp puoi inviare a chi vuoi un messaggio in automatico ogni X minuti.

## Istruzioni veloci

Richiede Google Chrome installato

### Windows - eseguibile exe (non necessita di Python installato)
Per Windows è possibile scaricare direttamente l'eseguibile situato nella cartella /dist/msuki.exe di questo progetto.
Per poterlo eseguire è necessario consentire l'utilizzo delle funzionalità di rete (verrà chiesto da Windows al primo avvio),   
e selezionare il comando ESEGUI COMUNQUE su Windows (perchè il programma non è firmato).

![alt text](msuki_1.PNG)
prima cliccare su *ulteriori informazioni*

![alt text](msuki_2.PNG)
poi cliccare su *esegui comunque*

una volta eseguito il software aprirà una finestra del terminale e una finestra di Chrome sul sito Whatsapp Web.
Per cominciare fate l'accesso a Whatsapp Web tramite la scansione del QR Code.

Successivamente passate alla finestra del terminale e inserite, seguendo le istruzioni a video:
Nome della persona/gruppo a cui inviare il messaggio (come lo avete salvato su whatsapp) + INVIO
Testo del messaggio + INVIO

controllate di aver effettuato correttamente l'accesso a whatsapp web con QR code e premete INVIO per avviare.

non chiudete la finestra del browser ne quella del terminale.

### Windows/Mac/Linux - esecuzione da terminale tramite Python
#### Download Python [](https://www.python.org/downloads/)

Avviare tramite PowerShell/terminale lo script 
1) spostarsi nella cartella dove è il file msuki.py (ad esempio tramite comando cd /Desktop)
2) installare le librerie necessarie al funzionamento del programma: 
**pip install selenium** 
**pip install webdriver_manager.chrome** 
3) eseguire il comando **python msuki.py** (se non funziona provare con py/python3 al posto di python)



esempio:

![alt text](msuki_3.PNG)